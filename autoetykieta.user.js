// ==UserScript==
// @name         Auto etykiety
// @namespace    http://tampermonkey.net/
// @version      1.3
// @description  try to take over the world!
// @author       Łysy
// @match        *://*.plemiona.pl/game.php*screen=overview_villages*mode=incomings*subtype=attacks*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    // Your code here...
})();

let ILE_SEKUND = Math.floor(Math.random() * 300) + 180//co tyle sekund skrypt bedzie refreshowal strone, czestsze odswiezanie moze ztriggerowac captcha

let timerek
let petelka = sessionStorage.getItem('loop') || 0
let soundalarm = sessionStorage.getItem('soundalarm') || 0
let i = ILE_SEKUND
let dodane_wioski = sessionStorage.getItem("dodane") || 0


$("#content_value").prepend(`<div><button id="turn_on">Turn on</button>&nbsp;&nbsp;<button id="turn_off">Turn off</button>&nbsp;&nbsp;<button id="alarm_start">Start Sound</button>&nbsp;&nbsp;<button id="alarm_stop">Stop Sound</button></div><div>Loop is set to ${petelka}. Sound effect is set to ${soundalarm}</div><div id="log">Dodalem ${dodane_wioski} etykiet</div><div id="counter"></div>`)

    var audioElement = document.createElement('audio');
    audioElement.setAttribute('src', 'https://assets.mixkit.co/sfx/preview/mixkit-security-facility-breach-alarm-994.mp3');

    audioElement.addEventListener('ended', function() {
        this.play();
    }, false);
    var alertH = ['00', '01', '02', '03', '04', '05', '06', '07'];
    var szlachta = $("#incomings_table").find("td:first-child:contains('Szlachcic')");
console.log(szlachta.length + " " + soundalarm);
    if(szlachta.length >= 2 && soundalarm > 0){
        var y;
        console.log("true");
        for (y = 0; y < szlachta.length-1; ++y) {
            console.log("petla szlachta " + y);
            console.log(szlachta[y].parentElement.children[5].innerText.slice(-12, -10))
            console.log(alertH.includes(szlachta[y].parentElement.children[5].innerText.slice(-12, -10)))
            if(alertH.includes(szlachta[y].parentElement.children[5].innerText.slice(-12, -10)) && alertH.includes(szlachta[y+1].parentElement.children[5].innerText.slice(-12, -10))){
                console.log("Play");
                audioElement.play();
            }
        }
    }
console.log($("#incomings_table").find("td:first-child:contains('Atak')").length)

setTimeout(function() {
    if ($("#incomings_table").find("td:first-child:contains('Atak')").length > 0) {
        let temp_dodane = sessionStorage.getItem("dodane") || 0
        sessionStorage.setItem("dodane",parseInt(temp_dodane)+$("#incomings_table").find("td:first-child:contains('Atak')").length)
        $(document.body).find("#select_all").trigger('click')
        setTimeout(function() {;$("input[value='Etykieta']").trigger('click')},2000);
    }
},2000)

if (petelka==1) {
    setInterval(function() {$("#counter").text(i--)},1000)
    timerek = setTimeout(function() {
        location.reload();
    }, ILE_SEKUND * 1000)
}

$("#alarm_start").click(function(){
  sessionStorage.setItem('soundalarm',1)
  location.reload();
});

$("#alarm_stop").click(function(){
  sessionStorage.setItem('soundalarm',0)
  location.reload();
});

$(document.body).on('click','#turn_on',function() {
    sessionStorage.setItem('loop',1)
    setInterval(function() {$("#counter").text(i--)},1000)
    location.reload();
    })


$(document.body).on('click','#turn_off',function() {
    sessionStorage.setItem('loop',0)
    clearTimeout(timerek)
})
;
