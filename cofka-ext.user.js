// ==UserScript==
// @name         Klin z cofki - plac
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://*.plemiona.pl/game.php?screen=place&village=*
// @match        https://*.plemiona.pl/game.php?village=*&screen=place*
// @match        https://*.klanhaboru.hu/game.php?village=*&screen=place*
// @match        https://*.klanhaboru.hu/game.php?screen=place&village=*
// @match        *.klanhaboru.hu/game.php?*
// @match        https://*.divokekmeny.cz/game.php?village=*&screen=place*
// @match        https://*.divokekmeny.cz/game.php?screen=place&village=*
// @match        *.divokekmeny.cz/game.php?*
// @match        *.plemiona.pl/game.php?*
// @match        *.plemiona.pl/game.php?screen=place&*
// @match        https://*/game.php?village=*&screen=place*
// @match        https://*/game.php?screen=place&village=*
// @match        */game.php?*
// @grant        none
// ==/UserScript==

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
const getDate = date => {
    return {
        h: date.getHours().toString().padStart(2, 0),
        m: date.getMinutes().toString().padStart(2, 0),
        s: date.getSeconds().toString().padStart(2, 0),
        ms: date.getMilliseconds().toString().padStart(3, 0)
    }
}

async function watch(wantedDate, selector) {
    await sleep(wantedDate.getTime() - Timing.getCurrentServerTime() + 30);
    console.log('Wysłałem o: ', Date.now());
    console.log('Miałem wysłać o: ', wantedDate.getTime());
    selector.click();
}

(function() {
    'use strict';
    document.querySelectorAll('.command-row .quickedit-label').forEach(command => {
        let date = command.innerText;
        date = date.trim();
        let isValid = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(date);
        if(isValid) {
            let h = +date.substring(0, 2);
            let m = +date.substring(3, 5);
            let sec = +date.substring(6, 8);
            let ms = 250;
            let day = (new Date()).getDate();
            let month = (new Date()).getMonth();
            let year = (new Date()).getFullYear();
            let givenDate = new Date(year, month, day, h, m, sec, ms);
            console.log('Data o ktorej przerwe atak:');
            console.log(givenDate);

            const commandRow = command.closest('.command-row');
            const td2 = commandRow.children[2];
            const selector = td2.querySelector('a');
            watch(givenDate, selector);
        console.log(date, isValid);
        }
    })
})();
